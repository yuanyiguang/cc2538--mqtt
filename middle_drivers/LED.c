//*****************************************************************************
//! @file       bsp_led.c
//! @brief      LED board support package for CCxxxx Cortex devices on
//!             SmartRF06 Battery/Evaluation Board.
//!
//! Revised     $Date: 2013-04-11 19:41:57 +0200 (Thu, 11 Apr 2013) $
//! Revision    $Revision: 9707 $
//
//  Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
//
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//****************************************************************************/
#ifndef BSP_LED_EXCLUDE


/**************************************************************************//**
* @addtogroup bsp_led_api
* @{
******************************************************************************/


/******************************************************************************
* INCLUDES
*/
#include "LED.h"

//
///**************cc2538LED******************************
//      功能：初始化LED
///**************cc2538LEDinit()函数*******************/

 void rt_hw_led_init(void)
  {
      GPIOPinTypeGPIOOutput(BSP_LED_BASE, BSP_LED_ALL);
      GPIOPinWrite(BSP_LED_BASE, BSP_LED_ALL, BSP_LED_ALL);
  }
/**************cc2538LEDon()函数*******************
      功能：打开LED
*****************************************************/

void rt_hw_led_off(rt_uint8_t num)
{
    rt_uint8_t uin8Leds;
    RT_ASSERT(num < LEDS_MAX_NUMBER);
    
    switch (num)
    {
        case 1:
            uin8Leds = BSP_LED_1;
        break;
        case 2:
            uin8Leds = BSP_LED_2;
        break;
        case 3:
            uin8Leds = BSP_LED_3;        
        break;
        default:
        break;
    }	
     GPIOPinWrite(BSP_LED_BASE, uin8Leds, uin8Leds);
}

/**************cc2538LEDoff()函数*******************
      功能：关闭LED
***************************************************/

void rt_hw_led_on(rt_uint8_t num)
{
    rt_uint8_t uin8Leds;
    RT_ASSERT(num < LEDS_MAX_NUMBER);
    
    switch (num)
    {
        case 1:
            uin8Leds = BSP_LED_1;
        break;
        case 2:
            uin8Leds = BSP_LED_2;
        break;
        case 3:
            uin8Leds = BSP_LED_3;        
        break;
        default:
        break;
    }	
     GPIOPinWrite(BSP_LED_BASE, uin8Leds, 0);
}


/**************cc2538LEDoff()函数*******************
      功能：翻转LED
***************************************************/

void bspLedToggle(uint8_t uin8Leds)
{
    //
    // Get current pin values of selected bits
    //
    uint32_t ui32Toggle = GPIOPinRead(BSP_LED_BASE, uin8Leds);

    //
    // Invert selected bits
    //
    ui32Toggle = (~ui32Toggle) & uin8Leds;

    //
    // Set GPIO
    //
    GPIOPinWrite(BSP_LED_BASE, uin8Leds, ui32Toggle);
}

void rt_hw_led_toggle(rt_uint8_t num)
{
    rt_uint8_t uin8Leds;
    RT_ASSERT(num < LEDS_MAX_NUMBER);
    
    switch (num)
    {
        case 1:
            uin8Leds = BSP_LED_1;
        break;
        case 2:
            uin8Leds = BSP_LED_2;
        break;
        case 3:
            uin8Leds = BSP_LED_3;        
        break;
        default:
        break;
    }	
     bspLedToggle(uin8Leds);
}

#ifdef RT_USING_FINSH
#include <finsh.h>
static rt_uint8_t led_inited = 0;
void led(rt_uint32_t led, rt_uint32_t value)
{
    /* init led configuration if it's not inited. */
    if (!led_inited)
    {
        rt_hw_led_init();
        led_inited = 1;
    }

    if ( led == 1 )
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(1);
            break;
        case 1:
            rt_hw_led_on(1);
            break;
        default:
            break;
        }
    }

    if ( led == 2 )
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(2);
            break;
        case 1:
            rt_hw_led_on(2);
            break;
        default:
            break;

        }
    }
      if ( led == 3 )
    {
        /* set led status */
        switch (value)
        {
        case 0:
            rt_hw_led_off(3);
            break;
        case 1:
            rt_hw_led_on(3);
            break;
        default:
            break;

        }
    }
}
FINSH_FUNCTION_EXPORT(led, set led[1 - 3] on[1] or off[0].)
#endif



/**************************************************************************//**
* Close the Doxygen group.
* @}
******************************************************************************/
#endif // #ifndef BSP_LED_EXCLUDE
