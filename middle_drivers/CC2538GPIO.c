/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *
 * Change Logs:
 * Date           Author            Notes
 * 2017-10-20     ZYH            the first version
 * 2017-11-15     ZYH            update to 3.0.0
 */

#include <rthw.h>
#include <rtdevice.h>
#include <board.h>
#include <rtthread.h>
  
#include "CC2538gpio.h"

#define NULL_PIN 0

extern void ioPinIntRegister(uint32_t ui32Base, uint8_t ui8Pins,
                             void (*pfnIntHandler)() );
extern void ioPinIntUnregister(uint32_t ui32Base, uint8_t ui8Pins);

struct rt_pin
{
  rt_uint32_t uPort;
  rt_uint8_t  uPins;
};

struct rt_pin rt_pin_tab[]=
{
  {GPIO_A_BASE,GPIO_PIN_0},
  {GPIO_A_BASE,GPIO_PIN_1},
  {GPIO_A_BASE,GPIO_PIN_2},
  {GPIO_A_BASE,GPIO_PIN_3},
  {GPIO_A_BASE,GPIO_PIN_4},
  {GPIO_A_BASE,GPIO_PIN_5},
  {GPIO_A_BASE,GPIO_PIN_6},
  {GPIO_A_BASE,GPIO_PIN_7}, 
  {GPIO_B_BASE,GPIO_PIN_0},
  {GPIO_B_BASE,GPIO_PIN_1},
  {GPIO_B_BASE,GPIO_PIN_2},
  {GPIO_B_BASE,GPIO_PIN_3},
  {GPIO_B_BASE,GPIO_PIN_4},
  {GPIO_B_BASE,GPIO_PIN_5},
  {GPIO_B_BASE,GPIO_PIN_6},
  {GPIO_B_BASE,GPIO_PIN_7}, 
  {GPIO_C_BASE,GPIO_PIN_0},
  {GPIO_C_BASE,GPIO_PIN_1},
  {GPIO_C_BASE,GPIO_PIN_2},
  {GPIO_C_BASE,GPIO_PIN_3},
  {GPIO_C_BASE,GPIO_PIN_4},
  {GPIO_C_BASE,GPIO_PIN_5},
  {GPIO_C_BASE,GPIO_PIN_6},
  {GPIO_C_BASE,GPIO_PIN_7}, 
  {GPIO_D_BASE,GPIO_PIN_0},
  {GPIO_D_BASE,GPIO_PIN_1},
  {GPIO_D_BASE,GPIO_PIN_2},
  {GPIO_D_BASE,GPIO_PIN_3},
  {GPIO_D_BASE,GPIO_PIN_4},
  {GPIO_D_BASE,GPIO_PIN_5},
  {GPIO_D_BASE,GPIO_PIN_6},
  {GPIO_D_BASE,GPIO_PIN_7}, 
};

struct rt_pin_irq_hdr pin_irq_hdr_tab[] =
{
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
};
void GPIO_Init(uint32_t ui32Port,uint8_t ui8Pins)
{   
    GPIOPinIntEnable(ui32Port, ui8Pins); 
    if(ui32Port==GPIO_A_BASE){ IntEnable(INT_GPIOA); }
        else if(ui32Port==GPIO_B_BASE){ IntEnable(INT_GPIOB); }
        else if(ui32Port==GPIO_C_BASE){ IntEnable(INT_GPIOC); }
        else if(ui32Port==GPIO_D_BASE){ IntEnable(INT_GPIOD); }
}

void stm32_pin_mode(rt_device_t dev, rt_base_t pin, rt_base_t mode)
{
    uint32_t ui32Port;
     uint8_t upin;
     uint8_t ui8Pins;

    upin =(uint8_t)pin;
    ui32Port= rt_pin_tab[upin].uPort;  
    ui8Pins=rt_pin_tab[upin].uPins;
   
    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_OE);
//        IOCPinConfigPeriphOutput(ui32Port, ui8Pins,IOC_MUX_OUT_SEL_GPT3_ICP1);
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_OE);
    }				
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
       GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PDE);
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
    GPIO_Init(ui32Port,ui8Pins);
    
}



void stm32_pin_write(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    uint32_t ui32Port;
    uint8_t upin;
    uint8_t ui8Pins;

    upin =(uint8_t)pin;
    ui32Port= rt_pin_tab[upin].uPort;  
    ui8Pins=rt_pin_tab[upin].uPins;
    
    if (value == PIN_LOW)
    {
//      
           GPIOIntTypeSet(ui32Port,ui8Pins,GPIO_FALLING_EDGE);
//         GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
           GPIOPinWrite(ui32Port, ui8Pins, 0);
    }
    else
    {
//        
         GPIOIntTypeSet(ui32Port,ui8Pins,GPIO_RISING_EDGE); 
         GPIOPinWrite(ui32Port,ui8Pins,ui8Pins);
    }  
}

int stm32_pin_read(rt_device_t dev, rt_base_t pin)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint8_t upin;
    int value;

    value = PIN_LOW;
    upin =(uint8_t)pin;
    ui32Port= rt_pin_tab[upin].uPort;  
    ui8Pins=rt_pin_tab[upin].uPins;

    if (GPIOPinRead(ui32Port,ui8Pins))
    {
        value = PIN_LOW;
    }
    else
    {
        value = PIN_HIGH;
    }
    return value;
}



rt_err_t stm32_pin_attach_irq(struct rt_device *device, rt_int32_t pin,
                  rt_uint32_t mode, void (*hdr)(), void *args)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;  
    uint8_t upin;
      
    upin =(uint8_t)pin;
    ui32Port= rt_pin_tab[upin].uPort;  
    ui8Pins=rt_pin_tab[upin].uPins;

    pin_irq_hdr_tab[upin].pin = pin;
    pin_irq_hdr_tab[upin].hdr = hdr;
    pin_irq_hdr_tab[upin].mode = mode;
    pin_irq_hdr_tab[upin].args = args; 
    
    GPIOPinIntEnable(ui32Port, ui8Pins );
    
    ioPinIntRegister(ui32Port,ui8Pins,pin_irq_hdr_tab[upin].hdr);

    return RT_EOK;   
 
}
rt_err_t stm32_pin_detach_irq(struct rt_device *device, rt_int32_t pin)
{
    rt_base_t level;
    uint32_t ui32Port;
    uint8_t ui8Pins;  
    uint8_t upin;
    upin =(uint8_t)pin;
    ui32Port= rt_pin_tab[upin].uPort;  
    ui8Pins=rt_pin_tab[upin].uPins;
   
    level = rt_hw_interrupt_disable();
    
    pin_irq_hdr_tab[upin].pin = -1;
    pin_irq_hdr_tab[upin].hdr = RT_NULL;
    pin_irq_hdr_tab[upin].mode = 0;
    pin_irq_hdr_tab[upin].args = RT_NULL;

    ioPinIntUnregister(ui32Port, ui8Pins);
    rt_hw_interrupt_enable(level);
    return RT_EOK;
}

rt_err_t stm32_pin_irq_enable(struct rt_device *device, rt_base_t pin,
                              rt_uint32_t enabled)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;  
    rt_base_t level;
    rt_int32_t irqindex = -1;
    uint8_t upin;
      
    upin =(uint8_t)pin;
    ui32Port= rt_pin_tab[upin].uPort;  
    ui8Pins=rt_pin_tab[upin].uPins;  

    if (enabled == PIN_IRQ_ENABLE)
    {
       
        level = rt_hw_interrupt_disable();
        if (pin_irq_hdr_tab[irqindex].pin == -1)
        {
            rt_hw_interrupt_enable(level);
            return RT_ENOSYS;
        }
            
        switch (pin_irq_hdr_tab[irqindex].mode)
        {
        case PIN_IRQ_MODE_RISING:
            GPIOIntTypeSet(ui32Port, ui8Pins, GPIO_RISING_EDGE);
             break;
        case PIN_IRQ_MODE_FALLING:
            GPIOIntTypeSet(ui32Port, ui8Pins, GPIO_FALLING_EDGE);
                break;
        case PIN_IRQ_MODE_RISING_FALLING:
            GPIOIntTypeSet(ui32Port, ui8Pins, GPIO_BOTH_EDGES);
             break;
        }
              
    
       rt_hw_interrupt_enable(level);
        
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
         GPIOPinIntDisable(ui32Port, ui8Pins);
    }
    else
    {
        return RT_ENOSYS;
    }
    
    return RT_EOK;
}

const static struct rt_pin_ops _stm32_pin_ops =
{
     stm32_pin_mode,
     stm32_pin_write,
     stm32_pin_read,
     stm32_pin_attach_irq,
     stm32_pin_detach_irq,
     stm32_pin_irq_enable,
};

int stm32_hw_pin_init(void)
{
    int result;
    SysCtrlClockSet(false, false, SYS_CTRL_SYSDIV_32MHZ);
    
    SysCtrlIOClockSet(SYS_CTRL_SYSDIV_32MHZ);
       
    GPIOPinTypeGPIOOutput(GPIO_C_BASE,GPIO_PIN_2);
    GPIOPinTypeGPIOOutput(GPIO_C_BASE,GPIO_PIN_0);
    GPIOPinTypeGPIOOutput(GPIO_C_BASE,GPIO_PIN_1);
    
    result = rt_device_pin_register("pin", &_stm32_pin_ops, RT_NULL);
    
    
    return result;
}






  