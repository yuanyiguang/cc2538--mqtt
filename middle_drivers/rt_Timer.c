#include <stdbool.h>
#include <stdint.h>
#include "hw_ints.h"
#include "hw_memmap.h"
#include "gpio.h"
#include "interrupt.h"
#include "ioc.h"
#include "hw_ioc.h"
#include "sys_ctrl.h"
#include <rtthread.h>
#include <rtdevice.h>
#include "gptimer.h"
#include "hwtimer.h"
#include "rt_timer.h"

rt_uint32_t  g_ui32Counter = 0;
rt_uint32_t num = 0;

struct rt_hwtimer_device timer0B;
struct cc2538_timer 
{
   uint8_t  Timerbuffer[4];
};

//struct rt_timer_tab[]=
//{
//  {GPIO_A_BASE,GPIO_PIN_0},
//  {GPIO_A_BASE,GPIO_PIN_1},
//  {GPIO_A_BASE,GPIO_PIN_2},
//  {GPIO_A_BASE,GPIO_PIN_3},
//
//};

void timer_init(struct rt_hwtimer_device *timer, rt_uint32_t state)
{
   RT_ASSERT(timer != RT_NULL);
   
   SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_GPT0); 
   
   if(state == 1)
   {
     TimerConfigure(GPTIMER0_BASE, GPTIMER_CFG_SPLIT_PAIR | 
                   GPTIMER_CFG_A_ONE_SHOT | GPTIMER_CFG_B_ONE_SHOT);
   }
   else if(state == 0)
   {
       TimerDisable(GPTIMER0_BASE , GPTIMER_B);
   }
   else 
   {
     
	   TimerConfigure(GPTIMER0_BASE, GPTIMER_CFG_SPLIT_PAIR | 
                   GPTIMER_CFG_A_PERIODIC   |GPTIMER_CFG_B_PERIODIC);
   }
}

rt_err_t timer_control(struct rt_hwtimer_device *timer, rt_uint32_t cmd, void *args)
{
  RT_ASSERT(timer != RT_NULL);
  
  rt_hwtimer_mode_t *m;
  
  switch (cmd)
    {
        /* 计数频率 */
    case HWTIMER_CTRL_FREQ_SET:
	  
	    m = (rt_hwtimer_mode_t*)args;
        //计数频率1MS
	    TimerLoadSet(GPTIMER0_BASE, GPTIMER_B, SysCtrlClockGet() / 1000);  
        break;
		 /* 停止*/
    case HWTIMER_CTRL_STOP:
        TimerDisable(GPTIMER0_BASE , GPTIMER_B);
        break;
    case HWTIMER_CTRL_INFO_GET:
        //         TimerDisable(GPTIMER0_BASE , GPTIMER_B)
        break;
	case HWTIMER_CTRL_MODE_SET:
	      m = (rt_hwtimer_mode_t*)args;
         if(*m == HWTIMER_MODE_PERIOD)
		 {
		  TimerConfigure(GPTIMER0_BASE, GPTIMER_CFG_SPLIT_PAIR |
                   GPTIMER_CFG_A_ONE_SHOT | GPTIMER_CFG_B_PERIODIC);
		 }
		 else if(*m == HWTIMER_MODE_ONESHOT)
		 {
		  TimerConfigure(GPTIMER0_BASE, GPTIMER_CFG_SPLIT_PAIR | 
                 GPTIMER_CFG_A_ONE_SHOT | GPTIMER_CFG_B_ONE_SHOT);
		 }
        break;
	}
  return RT_EOK;
}

rt_uint32_t timer_count_get(struct rt_hwtimer_device *timer)
{
  RT_ASSERT(timer != RT_NULL);
  rt_uint32_t cnnt;
  
  cnnt = TimerLoadGet(GPTIMER0_BASE,GPTIMER_B);
  
  return cnnt;
}
void timer_stop(struct rt_hwtimer_device *timer)
{
     RT_ASSERT(timer != RT_NULL);
	 
	 TimerDisable(GPTIMER0_BASE , GPTIMER_B);
}
void Timer0BIntHandler(void)
{
    //
    // Clear the timer interrupt flag.
    //
    TimerIntClear(GPTIMER0_BASE, GPTIMER_TIMB_TIMEOUT);  
	//
    // Update the periodic interrupt counter.
    //
    g_ui32Counter++;

    //
    // Once NUMBER_OF_INTS interrupts have been received, turn off the
    // GPTimer0B interrupt.
	if(g_ui32Counter == num)
    {
        //
        // Disable the GPTimer0B interrupt.
        //
	 rt_interrupt_enter();

    rt_device_hwtimer_isr(&timer0B);

    /* leave interrupt */
    rt_interrupt_leave();
	     
//        IntDisable(INT_TIMER0B);
//
//        //
//        // Turn off GPTimer0B interrupt.
//        //    
//        TimerIntDisable(GPTIMER0_BASE, GPTIMER_TIMB_TIMEOUT);       
//
//        //
//        // Clear any pending interrupt flag.
//        //
//        TimerIntClear(GPTIMER0_BASE, GPTIMER_TIMB_TIMEOUT);         

        g_ui32Counter = 0;
    }	 
    //

}

rt_err_t timer_start(struct rt_hwtimer_device *timer, rt_uint32_t cnt, rt_hwtimer_mode_t mode)
{
   RT_ASSERT(timer != RT_NULL);
   TimerEnable(GPTIMER0_BASE , GPTIMER_B);
   
   if(mode == HWTIMER_MODE_PERIOD)
   {
     TimerConfigure(GPTIMER0_BASE, GPTIMER_CFG_SPLIT_PAIR | 
                   GPTIMER_CFG_A_PERIODIC |GPTIMER_CFG_B_PERIODIC);
   }
   else if(mode == HWTIMER_MODE_ONESHOT)
   {
      TimerConfigure(GPTIMER0_BASE, GPTIMER_CFG_SPLIT_PAIR | 
                   GPTIMER_CFG_A_ONE_SHOT | GPTIMER_CFG_B_ONE_SHOT);
   }
   
    TimerIntRegister(GPTIMER0_BASE, GPTIMER_B, Timer0BIntHandler);    
    
    //
    // Enable processor interrupts.
    //
    IntMasterEnable();     

    //
    // Configure the GPTimer0B interrupt for timer timeout.
    //    
    TimerIntEnable(GPTIMER0_BASE, GPTIMER_TIMB_TIMEOUT);    

    //
    // Enable the GPTimer0B interrupt on the processor (NVIC).
    //
    IntEnable(INT_TIMER0B);

    //
    // Enable GPTimer0B.
    //
    TimerEnable(GPTIMER0_BASE, GPTIMER_B);  
	
	num = cnt ;
	
	
	return RT_EOK;
   
}

static struct rt_hwtimer_ops timer_ops =
{
   timer_init,
   timer_start,
   timer_stop,
   timer_count_get,
   timer_control,
};

void rt_hw_timer_init(void)
{
    struct cc2538_timer* timer0;
   
//    timer =&timer0;
         
	timer0B.ops    = &timer_ops;
	
	rt_device_hwtimer_register(&timer0B, "timer0B", &timer0);
}


