/*
 * File      : CC2538_uart.c
 * Name      : GCC
 * Time      : 2020-11-24
 */


#include <rtthread.h>
#include <rtdevice.h>
#include "rt_uart.h"

#include <uart.h>               // Access to driverlib uart fns
#include <interrupt.h>          // Access to driverlib interrupt fns
#include <sys_ctrl.h>           // Access to driverlib sys_ctrl fns
#include <ioc.h>                // Access to driverlib IO control fns
#include <gpio.h>               // Access to driverlib gpio fns

#include <hw_ioc.h>             // Access to IOC defines
#include <hw_uart.h>            // Access to UART defines

#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_uart.h"
#include "debug.h"
#include "interrupt.h"

#include "rt_uart.h"
#include "key.h"

#include "udma.h"
#include "flash.h"
#include "hw_flash_ctrl.h"
#include "rom.h"


#define EXAMPLE_PIN_UART_RXD             GPIO_PIN_0 
#define EXAMPLE_PIN_UART_TXD            GPIO_PIN_1 
#define EXAMPLE_GPIO_BASE               GPIO_A_BASE
#define EXAMPLE_PIN_UART1_RXD            GPIO_PIN_2 
#define EXAMPLE_PIN_UART1_TXD            GPIO_PIN_3 
#define EXAMPLE1_GPIO_BASE               GPIO_A_BASE

uint8_t UARTRXbuffer[2];  
uint8_t UARTbuffer[2];  

extern void MFS0RX_IRQHandler(void);
extern void bspUart0IsrHandler(void);
extern void bspUart1IsrHandler(void);

//static void DMA_Configuration();

#if (defined(RT_USING_UART0))

struct stm32_uart
{    
   uint8_t  UARTbuffer;
};
/* UART0 device driver structure */
struct rt_serial_device serial0;

void MFS0RX_IRQHandler(void)
{
	/* enter interrupt */
	rt_interrupt_enter();
	rt_hw_serial_isr(&serial0, RT_SERIAL_EVENT_RX_IND);
	/* leave interrupt */
	rt_interrupt_leave();
}
#endif

struct rt_serial_device serial1;


static rt_err_t uart03_configure(struct rt_serial_device *serial, struct serial_configure *cfg)
{
	    RT_ASSERT(serial != RT_NULL);
		SysCtrlClockSet(false, false, SYS_CTRL_SYSDIV_32MHZ);
		SysCtrlIOClockSet(SYS_CTRL_SYSDIV_32MHZ);
		
#if (defined(RT_USING_UART0))
    SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_UART0);//外设启动
	UARTDisable(UART0_BASE);//禁止串口
	UARTIntDisable(UART0_BASE, 0x1FFF);//禁止串口中断
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);//串口0时钟设置
	/*设置PA0 PA1输出模式，用于模拟串口*/
	IOCPinConfigPeriphOutput(EXAMPLE_GPIO_BASE, EXAMPLE_PIN_UART_TXD, IOC_MUX_OUT_SEL_UART0_TXD);
    GPIOPinTypeUARTOutput(EXAMPLE_GPIO_BASE, EXAMPLE_PIN_UART_TXD); 
    IOCPinConfigPeriphInput(EXAMPLE_GPIO_BASE, EXAMPLE_PIN_UART_RXD, IOC_UARTRXD_UART0);
    GPIOPinTypeUARTInput(EXAMPLE_GPIO_BASE, EXAMPLE_PIN_UART_RXD);
	UARTConfigSetExpClk(UART0_BASE, SysCtrlClockGet(), 115200,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                         UART_CONFIG_PAR_NONE));
	UARTTxIntModeSet(UART0_BASE, UART_TXINT_MODE_FIFO);//串口发送中断模式设置，先进先出
	UARTFIFOLevelSet(UART0_BASE, UART_FIFO_TX4_8, UART_FIFO_RX4_8);//串口先进先出的模式设置
	
	UARTIntClear(UART0_BASE, 0x17FF);//串口中断清理
	UARTIntEnable(UART0_BASE, (UART_INT_RX | UART_INT_TX | UART_INT_RT));//串口中断触发模式
	
	UARTEnable(UART0_BASE);//启动串口0
#endif
        
#if (defined(RT_USING_UART1))
    SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_UART1);//外设启动
	UARTDisable(UART1_BASE);//禁止串口
	UARTIntDisable(UART1_BASE, 0x1FFF);//禁止串口中断
    UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);//串口0时钟设置
	/*设置PA0 PA1输出模式，用于模拟串口*/
	IOCPinConfigPeriphOutput(EXAMPLE1_GPIO_BASE, EXAMPLE_PIN_UART1_TXD, IOC_MUX_OUT_SEL_UART1_TXD);
    GPIOPinTypeUARTOutput(EXAMPLE1_GPIO_BASE, EXAMPLE_PIN_UART1_TXD); 
    IOCPinConfigPeriphInput(EXAMPLE1_GPIO_BASE, EXAMPLE_PIN_UART1_RXD, IOC_UARTRXD_UART1);
    GPIOPinTypeUARTInput(EXAMPLE1_GPIO_BASE, EXAMPLE_PIN_UART1_RXD);
	UARTConfigSetExpClk(UART1_BASE, SysCtrlClockGet(), 115200,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                         UART_CONFIG_PAR_NONE));
	UARTTxIntModeSet(UART1_BASE, UART_TXINT_MODE_FIFO);//串口发送中断模式设置，先进先出
	UARTFIFOLevelSet(UART1_BASE, UART_FIFO_TX4_8, UART_FIFO_RX4_8);//串口先进先出的模式设置
	
	UARTIntClear(UART1_BASE, 0x17FF);//串口中断清理
	UARTIntEnable(UART1_BASE, (UART_INT_RX | UART_INT_TX | UART_INT_RT));//串口中断触发模式
	
	UARTEnable(UART1_BASE);//启动串口1
#endif
//    DMA_Configuration();
    return RT_EOK;
}

/*
 * Uart控制
 */ 
static rt_err_t uart03_control(struct rt_serial_device *serial, int cmd, void *arg)
{
	RT_ASSERT(serial != RT_NULL);	
	
	switch (cmd)
	{
	case RT_DEVICE_CTRL_CLR_INT:
		/* disable rx irq */
//		UARTIntDisable(UART0_BASE, 0x1FFF); 
//		UARTIntDisable(UART1_BASE, 0x1FFF); 
		break;
	case RT_DEVICE_CTRL_SET_INT:
		/* enable rx irq */
		UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT); 
//		UARTIntEnable(UART1_BASE, UART_INT_RX | UART_INT_RT); 
		break;
	}
	return (RT_EOK);	
}
/*
 * Uart发送一个字符
 */ 
static int uart03_putc(struct rt_serial_device *serial, char c)
{        
	RT_ASSERT(serial != RT_NULL);
	char* uartname = serial->parent.parent.name;
	if(strcmp(uartname,"uart0") == 0)
	{
		UARTCharPut(UART0_BASE, c);
	}
	if(strcmp(uartname,"uart1") == 0)
	{
		UARTCharPut(UART1_BASE, c);
	}
	return (1);	
}

/*
 * Uart接收一个字符
 */ 
static int uart03_getc(struct rt_serial_device *serial)
{	
    bool bIntDisabled;
        
	int ch = -1;
	
	    RT_ASSERT(serial != RT_NULL);	
	    bIntDisabled = IntMasterDisable();
        if(!bIntDisabled)
        {
            IntMasterEnable();
        }
            
	char* uartname = serial->parent.parent.name;
	if(strcmp(uartname,"uart0") == 0)
	{
		ch = UARTCharGetNonBlocking(UART0_BASE);  
		return(ch);  
	}
	if(strcmp(uartname,"uart1") == 0)
	{
		ch = UARTCharGetNonBlocking(UART1_BASE);  
		return(ch);  
	}	
    
}

void bspUart0IsrHandler(void)
{
  
    //
    // Get status of enabled interrupts
    //
    uint32_t ui32IntBm = UARTIntStatus(UART0_BASE, 1);

    //
    // Clear flags handled by this handler
    //
    UARTIntClear(UART0_BASE, (ui32IntBm & 0xF0));

    //
    // RX or RX timeout
    //
    if(ui32IntBm & (UART_INT_RX | UART_INT_RT))
    {
//      
//       while(UARTCharsAvail(UART0_BASE))
//        {
////            buBufPushByte();
//        }
    rt_interrupt_enter();
	rt_hw_serial_isr(&serial0, RT_SERIAL_EVENT_RX_IND);
	
	/* leave interrupt */
	rt_interrupt_leave();
	
    }
   
}

void bspUart1IsrHandler(void)
{
  
    //
    // Get status of enabled interrupts
    //
    uint32_t ui32IntBm = UARTIntStatus(UART1_BASE, 1);

    //
    // Clear flags handled by this handler
    //
    UARTIntClear(UART1_BASE, (ui32IntBm & 0xF0));

    //
    // RX or RX timeout
    //
    if(ui32IntBm & (UART_INT_RX | UART_INT_RT))
    {
//      
//       while(UARTCharsAvail(UART0_BASE))
//        {
////            buBufPushByte();
//        }
    rt_interrupt_enter();
	rt_hw_serial_isr(&serial1, RT_SERIAL_EVENT_RX_IND);
	
	/* leave interrupt */
	rt_interrupt_leave();
	
    }
   
}

static struct rt_uart_ops uart03_ops =
{
	uart03_configure,
	uart03_control,
	uart03_putc,
	uart03_getc,
};

void rt_hw_serial_init(void)
{
  struct stm32_uart* uart;
  struct serial_configure config;

#if defined(RT_USING_UART0)
  
        config.baud_rate = BAUD_RATE_115200;
        config.bufsz     = RT_SERIAL_RB_BUFSZ;
	serial0.ops    = &uart03_ops;
	serial0.config = config;

	/* register UART0 device */
	rt_hw_serial_register(&serial0, "uart0",
		RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX | RT_DEVICE_FLAG_STREAM | RT_DEVICE_FLAG_DMA_RX,
		NULL);
#endif
	
#if defined(RT_USING_UART1)
  
    serial1.ops    = &uart03_ops;
    serial1.config = config;

    /* register UART4 device */
    rt_hw_serial_register(&serial1, "uart1",
                RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX | RT_DEVICE_FLAG_STREAM | RT_DEVICE_FLAG_DMA_RX,
                NULL);
#endif
}

