/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *
 * Change Logs:
 * Date           Author            Notes
 * 2017-10-20     ZYH            the first version
 * 2017-11-15     ZYH            update to 3.0.0
 */

#include <rthw.h>
#include <rtdevice.h>
#include <board.h>
#include <rtthread.h>
#include "CC2538gpio.h"

#ifdef RT_USING_PIN
#define __CC2538_PIN_DEFAULT 0

//
//static const rt_uint16_t pins(rt_uint16_t gpio_pin)
//{
//  switch(gpio_pin)
//  {
//    case 1:
//      #define EXAMPLE_GPIO_BASE       GPIO_A_BASE
//      #define EXAMPLE_GPIO_PIN        GPIO_PIN_1
//      #define EXAMPLE_INT_GPIO        INT_GPIOA
//    case 2:
//      #define EXAMPLE_GPIO_BASE       GPIO_A_BASE
//      #define EXAMPLE_GPIO_PIN        GPIO_PIN_2
//      #define EXAMPLE_INT_GPIO        INT_GPIOA
//    default:
//        break;
//  }    
//}

struct rt_pin_irq_hdr pin_irq_hdr_tab[] =
{
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},
    {-1, 0, RT_NULL, RT_NULL},                  
};

void stm32_pin_write(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    rt_uint16_t gpio_pin;
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    if (get_st_gpio(gpio_pin) == RT_NULL)
    {
        return;
    }
    switch(pin/10)
    {
         case 0:
           uint32_t ui32Port=GPIO_A_BASE; break;
         case 1:
           uint32_t ui32Port=GPIO_B_BASE; break;
         case 2:
           uint32_t ui32Port=GPIO_C_BASE; break;
         case 3:
           uint32_t ui32Port=GPIO_D_BASE; break;
         default:
            break;
    }
    switch(pin%10)
    {
         case 1:
           uint32_t ui8Pins=GPIO_PIN_0; break;
         case 2:
           uint32_t ui8Pins=GPIO_PIN_1; break;
         case 3:
           uint32_t ui8Pins=GPIO_PIN_2; break;
         case 4:
           uint32_t ui8Pins=GPIO_PIN_3; break;
         case 5:
           uint32_t ui8Pins=GPIO_PIN_4; break;
         case 6:
           uint32_t ui8Pins=GPIO_PIN_5; break;
         case 7:
           uint32_t ui8Pins=GPIO_PIN_6; break;
         case 8:
           uint32_t ui8Pins=GPIO_PIN_7; break;
         default:
             break;
    }  
    switch(value)
    {
         case PIN_IRQ_MODE_RISING:
           uint32_t ui32IntType=GPIO_RISING_EDGE; break;
         case PIN_IRQ_MODE_FALLING:
           uint32_t ui32IntType=GPIO_FALLING_EDGE; break;
         case PIN_IRQ_MODE_HIGH_LEVEL:
           uint32_t ui32IntType=GPIO_HIGH_LEVEL; break;
         case PIN_IRQ_MODE_LOW_LEVEL:
           uint32_t ui32IntType=GPIO_LOW_LEVEL; break;
         default:
             break;
    }
      
    GPIOIntTypeSet(ui32Port,ui8Pins,ui32IntType);
}

void stm32_pin_read(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    int value;

    value = PIN_LOW;

    switch(pin/10)
    {
         case 0:
           uint32_t ui32Port=GPIO_A_BASE; break;
         case 1:
           uint32_t ui32Port=GPIO_B_BASE; break;
         case 2:
           uint32_t ui32Port=GPIO_C_BASE; break;
         case 3:
           uint32_t ui32Port=GPIO_D_BASE; break;
         default:
            break;
    }
    switch(pin%10)
    {
         case 1:
           uint32_t ui8Pins=GPIO_PIN_0; break;
         case 2:
           uint32_t ui8Pins=GPIO_PIN_1; break;
         case 3:
           uint32_t ui8Pins=GPIO_PIN_2; break;
         case 4:
           uint32_t ui8Pins=GPIO_PIN_3; break;
         case 5:
           uint32_t ui8Pins=GPIO_PIN_4; break;
         case 6:
           uint32_t ui8Pins=GPIO_PIN_5; break;
         case 7:
           uint32_t ui8Pins=GPIO_PIN_6; break;
         case 8:
           uint32_t ui8Pins=GPIO_PIN_7; break;
         default:
             break;
    }  
        
    if (!GPIOPinRead(ui32Port,ui8Pins))
    {
        value = PIN_LOW;
    }
    else
    {
        value = PIN_HIGH;
    }
}

void stm32_pin_mode(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    uint32_t ui32Interrupt;

    SysCtrlClockSet(false, false, SYS_CTRL_SYSDIV_32MHZ);
    SysCtrlIOClockSet(SYS_CTRL_SYSDIV_32MHZ);

    switch(pin/10)
    {
         case 0:
           uint32_t ui32Port=GPIO_A_BASE;
           ui32Interrupt=INT_GPIOA;
           break;
         case 1:
           uint32_t ui32Port=GPIO_B_BASE; 
           ui32Interrupt=INT_GPIOB;
           break;
         case 2:
           uint32_t ui32Port=GPIO_C_BASE;
           ui32Interrupt=INT_GPIOC;
           break;
         case 3:
           uint32_t ui32Port=GPIO_D_BASE;
           ui32Interrupt=INT_GPIOD;
           break;
         default:
            break;
    }
    switch(pin%10)
    {
         case 1:
           uint32_t ui8Pins=GPIO_PIN_0; break;
         case 2:
           uint32_t ui8Pins=GPIO_PIN_1; break;
         case 3:
           uint32_t ui8Pins=GPIO_PIN_2; break;
         case 4:
           uint32_t ui8Pins=GPIO_PIN_3; break;
         case 5:
           uint32_t ui8Pins=GPIO_PIN_4; break;
         case 6:
           uint32_t ui8Pins=GPIO_PIN_5; break;
         case 7:
           uint32_t ui8Pins=GPIO_PIN_6; break;
         case 8:
           uint32_t ui8Pins=GPIO_PIN_7; break;
         default:
             break;
    }  
        
    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }				
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
       GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PDE);
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
      

     GPIOPinIntEnable(ui32Port, ui8Pins);
    
     IntMasterEnable();
    
     IntEnable(ui32Interrupt);
}

rt_err_t stm32_pin_attach_irq(struct rt_device *device, rt_int32_t pin,
                  rt_uint32_t mode, void (*hdr)(void *args), void *args)
{
    rt_base_t level;
    rt_int32_t irqindex = -1;
    
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    uint32_t ui32Interrupt;
    uint32_t ui32GPIOIntStatus;

    switch(pin/10)
    {
         case 0:
           uint32_t ui32Port=GPIO_A_BASE;
           ui32Interrupt=INT_GPIOA;
           break;
         case 1:
           uint32_t ui32Port=GPIO_B_BASE; 
           ui32Interrupt=INT_GPIOB;
           break;
         case 2:
           uint32_t ui32Port=GPIO_C_BASE;
           ui32Interrupt=INT_GPIOC;
           break;
         case 3:
           uint32_t ui32Port=GPIO_D_BASE;
           ui32Interrupt=INT_GPIOD;
           break;
         default:
            break;
    }
    switch(pin%10)
    {
         case 1:
           uint32_t ui8Pins=GPIO_PIN_0; break;
         case 2:
           uint32_t ui8Pins=GPIO_PIN_1; break;
         case 3:
           uint32_t ui8Pins=GPIO_PIN_2; break;
         case 4:
           uint32_t ui8Pins=GPIO_PIN_3; break;
         case 5:
           uint32_t ui8Pins=GPIO_PIN_4; break;
         case 6:
           uint32_t ui8Pins=GPIO_PIN_5; break;
         case 7:
           uint32_t ui8Pins=GPIO_PIN_6; break;
         case 8:
           uint32_t ui8Pins=GPIO_PIN_7; break;
         default:
             break;
    }  
    
    ui32GPIOIntStatus = GPIOPinIntStatus(ui32Port, true);

    GPIOPinIntClear(ui32Port, ui32GPIOIntStatus);
}

    return RT_EOK;
}


const static struct rt_pin_ops _stm32_pin_ops =
{
    stm32_pin_mode,
    stm32_pin_write,
    stm32_pin_read,
    stm32_pin_attach_irq,
    stm32_pin_detach_irq,
    stm32_pin_irq_enable,
};

int stm32_hw_pin_init(void)
{
    int result;

    result = rt_device_pin_register("pin", &_stm32_pin_ops, RT_NULL);
    return result;
}
  