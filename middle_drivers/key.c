#include <rtthread.h>
#include <rtdevice.h>
#include "led.h"
#include "key.h"


#ifndef LED_DS
    #define LED_DS            16/* PC6 */
#endif
#ifndef LED_LCLK
    #define LED_LCLK         17/* PC6 */
#endif
#ifndef LED_SCK
    #define LED_SCK        18/* PC6 */
#endif
unsigned char  Tab[] = {0xc0,0xcf,0xa4,0xb0,0x99,0x92,0x82, 0xf8,0x80,
							0x90, 0x88,0x83,0xc6,0xa1,0x86,0x8e,0xff,0x7f};  

#ifndef KEY0_PIN_NUM
    #define KEY0_PIN_NUM            22 /* PC6 */
#endif
#ifndef KEY1_PIN_NUM
    #define KEY1_PIN_NUM          24/* PC6 */
#endif

//#ifndef PIN_LOW_T
//    #define PIN_LOW_T            25 /* PC6 */
//#endif
//#ifndef PIN_HIGH_T
//    #define PIN_HIGH_T         26/* PC6 */
//#endif

//extern void smg_set_value(uint32_t value);

rt_int8_t counter=0;


void beep_on(void *arge)
{    

      rt_kprintf("turn on beep!\n");
      rt_hw_led_on(2);
      counter++;
      rt_kprintf("counter  = %d\n",counter);    
//      smg_set_value(counter);
//      rt_pin_write(PIN_LOW_T,PIN_HIGH);
      
}
void beep_off(void *arge)
{    
      
      rt_kprintf("turn off beep!\n");
      rt_hw_led_on(3);  
      rt_hw_led_off(2); 
//      rt_thread_mdelay(10);
//      rt_pin_write(PIN_LOW_T,PIN_LOW);
     
}


void rt_hw_smg_init()
{
//    rt_pin_mode(LED_DS, PIN_MODE_OUTPUT);
//    rt_pin_mode(LED_LCLK, PIN_MODE_OUTPUT);
//    rt_pin_mode(LED_SCK, PIN_MODE_OUTPUT);
}

void SPI_595(uint8_t value)
{
	//595  C0  PC   Pb   5 6 7  load clk din
	uint8_t i;
	for(i=0;i<8;i++)  
	{
		rt_pin_write(LED_SCK, PIN_LOW);
		if(0x80&value)
			 rt_pin_write(LED_DS, PIN_HIGH);
		else
			 rt_pin_write(LED_DS, PIN_LOW);
		value = value <<1 ;
		  rt_pin_write(LED_SCK, PIN_HIGH);
              	rt_pin_write(LED_SCK, PIN_LOW);
	}
        rt_pin_write(LED_LCLK,PIN_HIGH);
        rt_pin_write(LED_LCLK,PIN_LOW );
}


static void smg_refresh_entry(void *param)
{

    while (1)
    {    
       	SPI_595(Tab[counter%10]);
        SPI_595(Tab[counter/10]);
        SPI_595(Tab[0]);
        SPI_595(Tab[0]);
//        rt_pin_write(LED_LCLK,PIN_HIGH );
        rt_thread_mdelay(500);
    }
}





static void pin_beep_sample(void *parameter)
{
//    /* 蜂鸣器引脚为输出模式 */
//     rt_pin_mode(PIN_LOW_T, PIN_MODE_OUTPUT);
//     rt_pin_mode(PIN_HIGH_T, PIN_MODE_OUTPUT);
//    /* 默认低电平 */
//    rt_pin_write(LED3, PIN_LOW);
       /* 按键0引脚为输入模式 */
    rt_pin_mode(KEY0_PIN_NUM, PIN_MODE_INPUT_PULLUP);
    /* 绑定中断，下降沿模式，回调函数名为beep_on */
    rt_pin_attach_irq(KEY0_PIN_NUM, PIN_IRQ_MODE_FALLING, beep_on, RT_NULL);
    
    rt_pin_irq_enable(KEY0_PIN_NUM, PIN_IRQ_ENABLE);
        /* 按键0引脚为输入模式 */
    
    rt_pin_mode(KEY1_PIN_NUM, PIN_MODE_INPUT_PULLUP);
    /* 绑定中断，下降沿模式，回调函数名为beep_on */
    rt_pin_attach_irq(KEY1_PIN_NUM, PIN_IRQ_MODE_FALLING, beep_off, RT_NULL);
    
    rt_pin_irq_enable(KEY1_PIN_NUM, PIN_IRQ_ENABLE);
 
       
}
static int smg_refresh_init()
{
    rt_thread_t tid3;

    tid3 = rt_thread_create("smg", smg_refresh_entry, RT_NULL, 1024, 25, 20);
    if (tid3 != RT_NULL)
    {
        rt_thread_startup(tid3);
    }
    else
    {
        rt_kprintf("smg thread create failed\n");
    }
    return 0;

}
INIT_APP_EXPORT(smg_refresh_init);

MSH_CMD_EXPORT(pin_beep_sample, pin beep sample);