/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *
 * Change Logs:
 * Date           Author            Notes
 * 2017-10-20     ZYH            the first version
 * 2017-11-15     ZYH            update to 3.0.0
 */

#include <rthw.h>
#include <rtdevice.h>
#include <board.h>
#include <rtthread.h>
  
#include "hw_memmap.h"
#include "hw_ioc.h"
#include "hw_ints.h"
#include "interrupt.h"
#include "gpio.h"
#include "ioc.h"
#include "uart.h"
#include "sys_ctrl.h"
#include "CC2538gpio.h"



#ifdef RT_USING_PIN
#define __CC2538_PIN_DEFAULT 0

static uint32_t get_pin(uint8_t pin)
{
    rt_uint16_t gpio_pin = __CC2538_PIN_DEFAULT;
    uint32_t ui32Port;
    
   switch(pin/10)
    {
         case 0:
            ui32Port=GPIO_A_BASE; break;
         case 1:
            ui32Port=GPIO_B_BASE; break;
         case 2:
            ui32Port=GPIO_C_BASE; break;
         case 3:
            ui32Port=GPIO_D_BASE; break;
         default:
            break;
    }

    return ui32Port;
};

static uint8_t get_pin2(uint8_t pin)
{
    rt_uint16_t gpio_pin = __CC2538_PIN_DEFAULT;
    
    uint8_t ui8Pins;
    switch(pin%10)
    {
         case 1:
            ui8Pins=GPIO_PIN_0; break;
         case 2:
            ui8Pins=GPIO_PIN_1; break;
         case 3:
            ui8Pins=GPIO_PIN_2; break;
         case 4:
            ui8Pins=GPIO_PIN_3; break;
         case 5:
            ui8Pins=GPIO_PIN_4; break;
         case 6:
            ui8Pins=GPIO_PIN_5; break;
         case 7:
            ui8Pins=GPIO_PIN_6; break;
         case 8:
            ui8Pins=GPIO_PIN_7; break;
         default:
             break;
    }  

    return ui8Pins;
};

void GPIO_Init(uint32_t ui32Port,uint8_t ui8Pins)
{
  
   SysCtrlClockSet(false, false, SYS_CTRL_SYSDIV_32MHZ);
   
   SysCtrlIOClockSet(SYS_CTRL_SYSDIV_32MHZ);
   
   GPIOIntTypeSet(ui32Port, ui8Pins, GPIO_FALLING_EDGE);

   GPIOPinIntEnable(ui32Port, ui8Pins);
   
   IntMasterEnable();
   
   if(ui32Port==GPIO_A_BASE){ IntEnable(INT_GPIOA); }
   else if(ui32Port==GPIO_B_BASE){ IntEnable(INT_GPIOB); }
   else if(ui32Port==GPIO_C_BASE){ IntEnable(INT_GPIOC); }
   else if(ui32Port==GPIO_D_BASE){ IntEnable(INT_GPIOD); }
}

void stm32_pin_write(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;

    ui32Port= get_pin(pin);
    ui8Pins= get_pin2(pin);
                      
    switch(value)
    {
         case PIN_IRQ_MODE_RISING:
            ui32IntType=GPIO_RISING_EDGE; break;
         case PIN_IRQ_MODE_FALLING:
            ui32IntType=GPIO_FALLING_EDGE; break;
         case PIN_IRQ_MODE_HIGH_LEVEL:
            ui32IntType=GPIO_HIGH_LEVEL; break;
         case PIN_IRQ_MODE_LOW_LEVEL:
            ui32IntType=GPIO_LOW_LEVEL; break;
         default:
             break;
    }
      
    GPIOIntTypeSet(ui32Port,ui8Pins,ui32IntType);
}

int stm32_pin_read(rt_device_t dev, rt_base_t pin)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    int value;

    value = PIN_LOW;
    
    ui32Port= get_pin(pin);
    ui8Pins= get_pin2(pin);
     
    if (!GPIOPinRead(ui32Port,ui8Pins))
    {
        value = PIN_LOW;
    }
    else
    {
        value = PIN_HIGH;
    }
    return value;
}

void stm32_pin_mode(rt_device_t dev, rt_base_t pin, rt_base_t mode)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    uint32_t ui32Interrupt;

    ui32Port= get_pin(pin);
    ui8Pins= get_pin2(pin);
        
    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }				
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
       GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PDE);
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
    GPIO_Init(ui32Port,ui8Pins);
    
}

//void GPIOAIntHandler(void)
//{
//    uint32_t ui32GPIOIntStatus;
//
//    //
//    // Get the masked interrupt status.
//    //
//    ui32GPIOIntStatus = GPIOPinIntStatus(EXAMPLE_GPIO_BASE, true);
//    //
//    // Acknowledge the GPIO  - Pin n interrupt by clearing the interrupt flag.
//    //
//    GPIOPinIntClear(EXAMPLE_GPIO_BASE, ui32GPIOIntStatus);
//}

rt_err_t stm32_pin_attach_irq(struct rt_device *device, rt_int32_t pin,
                  rt_uint32_t mode, void (*hdr)(void *args), void *args)
{
         rt_base_t level;
         uint32_t ui32Port;
         uint8_t ui8Pins;        
         
         ui32Port= get_pin(pin);
         ui8Pins= get_pin2(pin);
           
         level = rt_hw_interrupt_disable();
           
         ioPinIntRegister(ui32Port, ui8Pins, &hdr);
         
         rt_hw_interrupt_enable(level);
                     
         return RT_EOK;
}

const static struct rt_pin_ops _stm32_pin_ops =
{
     stm32_pin_mode,
     stm32_pin_write,
     stm32_pin_read,
     stm32_pin_attach_irq,
//    stm32_pin_detach_irq,
//    stm32_pin_irq_enable,
};

int stm32_hw_pin_init(void)
{
    int result;
    
    result = rt_device_pin_register("pin", &_stm32_pin_ops, RT_NULL);
    return result;
}
#endif

/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *
 * Change Logs:
 * Date           Author            Notes
 * 2017-10-20     ZYH            the first version
 * 2017-11-15     ZYH            update to 3.0.0
 */

#include <rthw.h>
#include <rtdevice.h>
#include <board.h>
#include <rtthread.h>
  
#include "CC2538gpio.h"
#define GPIO8  8
#define GPIO7  7

#define __STM32_PIN(index, gpio, gpio_index) (gpio | gpio_index)
#define __STM32_PIN_DEFAULT 0

#define A   (1U << 8)
#define B   (2U << 8)
#define C   (3U << 8)
#define D   (4U << 8)


static uint32_t get_st_gpio(rt_uint16_t gpio_pin)
{
    switch(gpio_pin & 0xFF00)
    {
    case A:
        #ifdef GPIOA
        return GPIOA;
        #endif
    case B:
        #ifdef GPIOB
        return GPIOB;
        #endif
    case C:
        #ifdef GPIOC
        return GPIOC;
        #endif
    case D:
        #ifdef GPIOD
        return GPIOD;
        #endif
    default:
        return RT_NULL;
    }
}

#define get_st_pin(gpio_pin) (0x01 << (gpio_pin&0xFF))

static const rt_uint16_t pins[] =
{
    __STM32_PIN_DEFAULT,
    __STM32_PIN_DEFAULT,
    __STM32_PIN_DEFAULT,
    __STM32_PIN_DEFAULT,
    __STM32_PIN(5, B, 0),
    __STM32_PIN(6, C, 7),
    __STM32_PIN(7, C, 6),
    __STM32_PIN(8, C, 5),
    __STM32_PIN(9, C, 4),
    __STM32_PIN_DEFAULT,
    __STM32_PIN(11, C, 3),
    __STM32_PIN(12, C, 2),
    __STM32_PIN(13, C, 1),
    __STM32_PIN(14, C, 0),
    __STM32_PIN_DEFAULT,
};

struct rt_pin_irq_hdr pin_irq_hdr_tab[] =
{
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
    { -1, 0, RT_NULL, RT_NULL},
};

void GPIO_Init(uint32_t ui32Port,uint8_t ui8Pins)
{
  
   SysCtrlClockSet(false, false, SYS_CTRL_SYSDIV_32MHZ);
   
   SysCtrlIOClockSet(SYS_CTRL_SYSDIV_32MHZ);
   
   GPIOPinIntEnable(ui32Port, ui8Pins);
   
   IntMasterEnable();
   
   if(ui32Port==GPIO_A_BASE){ IntEnable(INT_GPIOA); }
   else if(ui32Port==GPIO_B_BASE){ IntEnable(INT_GPIOB); }
   else if(ui32Port==GPIO_C_BASE){ IntEnable(INT_GPIOC); }
   else if(ui32Port==GPIO_D_BASE){ IntEnable(INT_GPIOD); }
}

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])
static rt_uint16_t get_pin(uint8_t pin)
{
    rt_uint16_t gpio_pin = __STM32_PIN_DEFAULT;
    if (pin < ITEM_NUM(pins))
    {
        gpio_pin = pins[pin];
    }
    return gpio_pin;
};

void stm32_pin_write(rt_device_t dev, rt_base_t pin, rt_base_t value)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    rt_uint16_t gpio_pin;
    gpio_pin = get_pin(pin);
    if (get_st_gpio(gpio_pin) == RT_NULL)
    {
        return;
    }       
   
    if(pin==GPIO8)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_5;      
    }   
    else if(pin==GPIO7)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_6;  
    }
    
     if (value == PIN_LOW)
    {
        GPIOIntTypeSet(ui32Port,ui8Pins,GPIO_LOW_LEVEL);
    }
    else
    {
         GPIOIntTypeSet(ui32Port,ui8Pins,GPIO_HIGH_LEVEL);
    }  
}

int stm32_pin_read(rt_device_t dev, rt_base_t pin)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    int value;

    value = PIN_LOW;
    
//    ui32Port= get_pin(pin);
//    ui8Pins= get_pin2(pin);
//     
    if (!GPIOPinRead(ui32Port,ui8Pins))
    {
        value = PIN_LOW;
    }
    else
    {
        value = PIN_HIGH;
    }
    return value;
}

void stm32_pin_mode(rt_device_t dev, rt_base_t pin, rt_base_t mode)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;
    uint32_t ui32IntType;
    uint32_t ui32Interrupt;
    rt_uint16_t gpio_pin;
    gpio_pin = get_pin(pin);
    if (get_st_gpio(gpio_pin) == RT_NULL)
    {
        return;
    }    
     if(pin==GPIO8)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_5;      
    } 
      else if(pin==GPIO7)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_6;  
    }
            
    if (mode == PIN_MODE_OUTPUT)
    {
        /* output setting */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
    else if (mode == PIN_MODE_OUTPUT_OD)
    {
        /* output setting: od. */
        GPIOPinTypeGPIOOutput(ui32Port, ui8Pins);
//        IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }				
    else if (mode == PIN_MODE_INPUT)
    {
        /* input setting: not pull. */
        GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PDE);
    }
    else if (mode == PIN_MODE_INPUT_PULLUP)
    {
        /* input setting: pull up. */
        GPIOPinTypeGPIOInput(ui32Port, ui8Pins);
       IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
    }
    GPIO_Init(ui32Port,ui8Pins);
    
}

rt_err_t stm32_pin_attach_irq(struct rt_device *device, rt_int32_t pin,
                  rt_uint32_t mode, void (*hdr)(void *args), void *args)
{
    uint32_t ui32Port;
    uint8_t ui8Pins;        
         
    rt_uint16_t gpio_pin;
    rt_base_t level;
    rt_int32_t irqindex = -1;
        if(pin==GPIO8)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_5;      
    } 
      else if(pin==GPIO7)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_6;  
    }
    gpio_pin = get_pin(pin);
    if (get_st_gpio(gpio_pin) == RT_NULL)
    {
        return RT_ENOSYS;
    }
    if (pin_irq_hdr_tab[irqindex].pin == pin &&
        pin_irq_hdr_tab[irqindex].hdr == hdr &&
        pin_irq_hdr_tab[irqindex].mode == mode &&
        pin_irq_hdr_tab[irqindex].args == args)
    {
        rt_hw_interrupt_enable(level);
        return RT_EOK;
    }
    if (pin_irq_hdr_tab[irqindex].pin != -1)
    {
        rt_hw_interrupt_enable(level);
        return RT_EBUSY;
    }
    pin_irq_hdr_tab[irqindex].pin = pin;
    pin_irq_hdr_tab[irqindex].hdr = hdr;
    pin_irq_hdr_tab[irqindex].mode = mode;
    pin_irq_hdr_tab[irqindex].args = args; 
    
    ioPinIntRegister(ui32Port, ui8Pins, &hdr);
    
    rt_hw_interrupt_enable(level);
  
       return RT_EOK;   
 
}
rt_err_t stm32_pin_detach_irq(struct rt_device *device, rt_int32_t pin)
{
    rt_uint16_t gpio_pin;
    rt_base_t level;
    uint32_t ui32Port;
    uint8_t ui8Pins;  
    
        if(pin==GPIO8)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_5;      
    } 
      else if(pin==GPIO7)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_6;  
    }
    rt_int32_t irqindex = -1;
    gpio_pin = get_pin(pin);
    if (get_st_gpio(gpio_pin) == RT_NULL)
    {
        return RT_ENOSYS;
    }
   
    level = rt_hw_interrupt_disable();
    if (pin_irq_hdr_tab[irqindex].pin == -1)
    {
        rt_hw_interrupt_enable(level);
        return RT_EOK;
    }
    pin_irq_hdr_tab[irqindex].pin = -1;
    pin_irq_hdr_tab[irqindex].hdr = RT_NULL;
    pin_irq_hdr_tab[irqindex].mode = 0;
    pin_irq_hdr_tab[irqindex].args = RT_NULL;
    ioPinIntUnregister(ui32Port, ui8Pins);
    rt_hw_interrupt_enable(level);
    return RT_EOK;
}

rt_err_t stm32_pin_irq_enable(struct rt_device *device, rt_base_t pin,
                              rt_uint32_t enabled)
{
  uint32_t ui32Port;
    uint8_t ui8Pins;  
    rt_uint16_t gpio_pin;
    rt_base_t level;
    rt_int32_t irqindex = -1;
      
        if(pin==GPIO8)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_5;      
    } 
      else if(pin==GPIO7)
    {
      ui32Port = GPIO_C_BASE;
      ui8Pins = GPIO_PIN_6;  
    }
    gpio_pin = get_pin(pin);
    if (get_st_gpio(gpio_pin) == RT_NULL)
    {
        return RT_ENOSYS;
    }
    if (enabled == PIN_IRQ_ENABLE)
    {
       GPIOPinIntEnable(ui32Port, ui8Pins);
        level = rt_hw_interrupt_disable();
        if (pin_irq_hdr_tab[irqindex].pin == -1)
        {
            rt_hw_interrupt_enable(level);
            return RT_ENOSYS;
        }
       
        GPIOPinIntEnable(ui32Port, ui8Pins);
        
        switch (pin_irq_hdr_tab[irqindex].mode)
        {
        case PIN_IRQ_MODE_RISING:
          
            IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PDE);
            break;
        case PIN_IRQ_MODE_FALLING:
           IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_PUE);
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            IOCPadConfigSet(ui32Port, ui8Pins, IOC_OVERRIDE_OE);
            break;
        }
        rt_hw_interrupt_enable(level);
        GPIOPinIntEnable(ui32Port, ui8Pins);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
         GPIOPinIntDisable(ui32Port, ui8Pins);
    }
    else
    {
        return RT_ENOSYS;
    }
    return RT_EOK;
}

const static struct rt_pin_ops _stm32_pin_ops =
{
     stm32_pin_mode,
     stm32_pin_write,
     stm32_pin_read,
     stm32_pin_attach_irq,
    stm32_pin_detach_irq,
    stm32_pin_irq_enable,
};

int stm32_hw_pin_init(void)
{
    int result;
    
    result = rt_device_pin_register("pin", &_stm32_pin_ops, RT_NULL);
    return result;
}

  

  