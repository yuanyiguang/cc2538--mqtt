/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-09-22     Bernard      add board.h to this bsp
 */

// <<< Use Configuration Wizard in Context Menu >>>
#ifndef __BOARD_H__
#define __BOARD_H__

#include "bsp.h"
#include "core_cm3.h"

/* board configuration */

/* whether use board external SRAM memory */

// <o> Internal SRAM memory size[Kbytes] <8-64>
//	<i>Default: 64
#define CC2538_SRAM_SIZE         32
#define CC2538_SRAM_END          (0x20000000  + CC2538_SRAM_SIZE * 1024)

void rt_hw_board_init(void);

#endif /* __BOARD_H__ */
