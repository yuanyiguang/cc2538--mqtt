/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      first implementation
 * 2013-07-12     aozima       update for auto initial.
 */

#include <rthw.h>
#include <rtthread.h>


#include "board.h"
#include "bsp.h"


#include "core_cm3.h"
#include "uart.h"
#include "rt_uart.h"
#include "ioc.h"
#include "gpio.h"
#include "sys_ctrl.h"
#include "hw_ioc.h"

#include <uart.h>               // Access to driverlib uart fns
#include <interrupt.h>          // Access to driverlib interrupt fns
#include <sys_ctrl.h>           // Access to driverlib sys_ctrl fns
#include <ioc.h>                // Access to driverlib IO control fns
#include <gpio.h>               // Access to driverlib gpio fns

#include <hw_uart.h>            // Access to UART defines


#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_uart.h"
#include "debug.h"
#include "interrupt.h"
#include "udma.h"
#include "display.h"
#include "CC2538gpio.h"
   
   
extern void appUartIsr(void);
extern int stm32_hw_pin_init();
extern void rt_hw_timer_init();

uint32_t SystemCoreClock = 32000000;
//
//void
//InitConsole(void)
//{
//    //
//    // Map UART signals to the correct GPIO pins and configure them as
//    // hardware controlled.
//    //
//    IOCPinConfigPeriphOutput(EXAMPLE_GPIO_UART_BASE, EXAMPLE_PIN_UART_TXD, 
//                             IOC_MUX_OUT_SEL_UART0_TXD);
//    GPIOPinTypeUARTOutput(EXAMPLE_GPIO_UART_BASE, EXAMPLE_PIN_UART_TXD);
//    
//    IOCPinConfigPeriphInput(EXAMPLE_GPIO_UART_BASE, EXAMPLE_PIN_UART_RXD, 
//                            IOC_UARTRXD_UART0);
//    GPIOPinTypeUARTInput(EXAMPLE_GPIO_UART_BASE, EXAMPLE_PIN_UART_RXD);
//     
//}
//static void SPICLKInit(uint32_t ui32SysClockSpeed);

/**
 * This is the timer interrupt service routine.
 *
 */
void SysTick_Handler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    rt_tick_increase();

    /* leave interrupt */
    rt_interrupt_leave();
}

/**
 * This function will initial STM32 board.
 */
void rt_hw_board_init(void)
{
    /* Configure the SysTick */
    SysTick_Config( SystemCoreClock / RT_TICK_PER_SECOND );
   
//    SPICLKInit(BSP_SPI_CLK_SPD);
    UARTIntRegister(UART0_BASE, &appUartIsr);
    UARTIntRegister(UART1_BASE, &appUartIsr);
    rt_hw_serial_init();
    stm32_hw_pin_init();
    rt_hw_timer_init();

    rt_hw_smg_init();
    
//    InitConsole();
    rt_console_set_device(RT_CONSOLE_DEVICE_NAME);
    

}

static void appUartIsr(void)
{
    uint32_t ulIntBm0 = UARTIntStatus(UART0_BASE, 1);
    uint32_t ulIntBm1 = UARTIntStatus(UART1_BASE, 1);
    //
    // Serve interrupts handled by BSP UART interrupt handler
    //
    if(ulIntBm0 & 0xF0)
    {
        bspUart0IsrHandler();
    }
    
    if(ulIntBm1 & 0xF0)
    {
        bspUart1IsrHandler();
    }
}


