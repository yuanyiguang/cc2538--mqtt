/*
 * File      : application.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2009 - 2011, RT-Thread Development Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2011-05-24     Bernard      the first version
 */

/**
 * @addtogroup FM3
 */
/*@{*/

#include <rtthread.h>
#include "board.h"
#include "LED.h"
#include "pin.h"
#include "MQTTPacket.h"

#define DEVICE_NAME1	"uart0"
#define DEVICE_NAME2	"uart1"

static rt_device_t serial0;
static rt_device_t serial1;

char uart0data[] = "uart0 test data!\r\n";
char uart1data[] = "uart1 test data!\r\n";

static struct rt_semaphore rx_sem;    /* 用于接收消息的信号量 */
static struct rt_semaphore ok_sem;      /*用于ESP8266模块指令返回成功的信号量*/
char* ESP8266_Data[] = {
  	"ATE0\r\n",
	"AT+CWAUTOCONN=0\r\n",
	"AT+CWMODE=1\r\n",
	"AT+CWJAP=\"YJ\",\"88888888\"\r\n",
	"AT+CIPSTART=\"TCP\",\"139.159.163.215\",9501\r\n",
	"AT+CIPMODE=1\r\n",
	"AT+CIPSEND\r\n"
};

rt_size_t ESP8266_Data_Length[] = {
  	6,
	17,
	13,
	26,
	42,
	14,
	12
};

bool isconn = false;

#ifndef LED3
    #define LED3            8  /* PC6 */
#endif

void rt_init_thread_entry(void *parameter)
{    
  bool conn = false;
  MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
  unsigned char buf[200];
  int buflen = sizeof(buf);
  MQTTString topicString = MQTTString_initializer;
  int req_qos = 0;
  int msgid = 1;
  char* payload = "yyg";
  int payloadlen = strlen(payload);
  int len = 0;
  char *host = "139.159.163.215";
  int port = 9501;
  
#ifdef RT_USING_COMPONENTS_INIT
    /* initialization RT-Thread Components */
    rt_components_init();
#endif

//#ifdef  RT_USING_FINSH
//    finsh_set_device(RT_CONSOLE_DEVICE_NAME);
//#endif  /* RT_USING_FINSH */

       /**< init led device */
        rt_hw_led_init();
        
        
  while(1)
  {

	rt_hw_led_toggle(1);
	rt_hw_led_toggle(2);
	rt_hw_led_toggle(3);
	
	if(!conn)
	{
	  for(int i = 0;i < 7;i++)
	  {
	    rt_device_write(serial1, 0, ESP8266_Data[i], ESP8266_Data_Length[i]);
	    rt_sem_take(&ok_sem, RT_WAITING_FOREVER);
            
	  }
	  conn = true; 
	}
	if(conn)
	{
//	rt_device_write(serial1, 0, "cmd=2&uid=e8dfdf8a01da2d490a6719c85e7d1301&topic=a&msg=1\r\n", 58);	
	  if(!isconn)
	  {

	    data.clientID.cstring = "e8dfdf8a01da2d490a6719c85e7d1301";
	    data.keepAliveInterval = 20;
	    data.cleansession = 1;
	    data.username.cstring = "";
	    data.password.cstring = "";
	    len = MQTTSerialize_connect(buf, buflen, &data);
//	    transport_sendPacketBuffer(mysock, buf, len);
	    rt_device_write(serial1, 0, buf, len);

//	    /* wait for connack */
//	    if (MQTTPacket_read(buf, buflen, transport_getdata) == CONNACK)
//	    {
//		    unsigned char sessionPresent, connack_rc;
//
//		    if (MQTTDeserialize_connack(&sessionPresent, &connack_rc, buf, buflen) != 1 || connack_rc != 0)
//		    {
//			rt_kprintf("Unable to connect!");
//		    }
//		    else
//		    	
//	    }
//	    else
//		    rt_kprintf("connect error!");
//	
		isconn = true;
	  }
	  else
	  {
	    /* subscribe */
	    topicString.cstring = "tdynode";	
//	    len = MQTTSerialize_subscribe(buf, buflen, 0, msgid, 1, &topicString, &req_qos);
	    len = MQTTSerialize_publish(buf, buflen, 0, 0, 0, 0, topicString, (unsigned char*)payload, payloadlen);
	    rt_device_write(serial1, 0, buf, len);
	  }
	}
	rt_thread_mdelay(2000);
  }
}

void rt_uartget_thread_entry(void *parameter)
{
    char ch;
    int count = 0;

    while (1)
    {
        /* 从串口读取一个字节的数据，没有读取到则等待接收信号量 */
        while (rt_device_read(serial1, -1, &ch, 1) != 1)
        {
          if(strcmp(rxData, "OK") == 0)
          {
            /*当ESP8266模块接受指令回复OK时，释放此信号量*/
            rt_sem_release(&ok_sem);
          }
            /* 阻塞等待接收信号量，等到信号量后再次读取数据 */
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
        }
        count++;
        /* 读取到的数据通过串口输出 */
//        rt_device_write(serial0, 0, &ch, 1);
        if(ch == "O")
        *rxData = ch;
        rxData++;
    }
}

/* 接收数据回调函数 */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    /* 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量 */
    rt_sem_release(&rx_sem);

    return RT_EOK;
}

int rt_application_init()
{
    rt_thread_t tid,tid1;
    
    serial0 = rt_device_find(DEVICE_NAME1);
    serial1 = rt_device_find(DEVICE_NAME2);
    
    rt_device_open(serial0,RT_DEVICE_FLAG_INT_RX);
    rt_device_open(serial1,RT_DEVICE_FLAG_INT_RX);
    
    /* 初始化信号量 */
    rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    rt_sem_init($ok_sem, "ok_sem", 0, RT_IPC_FLAG_FIFO);

    /* 设置接收回调函数 */
    rt_device_set_rx_indicate(serial1, uart_input);
    
    /*线程创建和启动*/

    tid = rt_thread_create("init",
                           rt_init_thread_entry, RT_NULL,
                           1024, RT_THREAD_PRIORITY_MAX/3, 20);
    if (tid != RT_NULL) rt_thread_startup(tid);

     tid1 = rt_thread_create("uartGet",
                           rt_uartget_thread_entry, RT_NULL,
                           1024, RT_THREAD_PRIORITY_MAX/4, 20);
    if (tid1 != RT_NULL) rt_thread_startup(tid1);

    return 0;
}

/*@}*/
