///*  CC2538��NVIC����  */
//
//#ifndef __CC2538_H
//#define __CC2538_H
//
//#ifdef __cplusplus
// extern "C" {
//#endif 
//   
//typedef enum IRQn
//{
//    
//    ResetISR,                               // 1 The reset handler
//    NmiISR,                                 // The NMI handler
//    FaultISR,                               // The hard fault handler
//    IntDefaultHandler,                      // 4 The MPU fault handler
//    IntDefaultHandler,                      // 5 The bus fault handler
//    IntDefaultHandler,                      // 6 The usage fault handler
//    0,                                      // 7 Reserved
//    0,                                      // 8 Reserved
//    0,                                      // 9 Reserved
//    0,                                      // 10 Reserved
//    IntDefaultHandler,                      // 11 SVCall handler
//    IntDefaultHandler,                      // 12 Debug monitor handler
//    0,                                      // 13 Reserved
//    PendSVIntHandler,                       // 14 The PendSV handler
//    SysTickIntHandler,                      // 15 The SysTick handler
//    GPIOA_IRQn,                        // 16 GPIO Port A
//    GPIOBIntHandler,                        // 17 GPIO Port B
//    GPIOCIntHandler,                        // 18 GPIO Port C
//    GPIODIntHandler,                        // 19 GPIO Port D
//    0,                                      // 20 none
//    UART0IntHandler,                        // 21 UART0 Rx and Tx
//    UART1IntHandler,                        // 22 UART1 Rx and Tx
//    SSI0IntHandler,                         // 23 SSI0 Rx and Tx
//    I2CIntHandler,                          // 24 I2C Master and Slave
//    0,                                      // 25 Reserved
//    0,                                      // 26 Reserved
//    0,                                      // 27 Reserved
//    0,                                      // 28 Reserved
//    0,                                      // 29 Reserved
//    ADCIntHandler,                          // 30 ADC Sequence 0
//    0,                                      // 31 Reserved
//    0,                                      // 32 Reserved
//    0,                                      // 33 Reserved
//    WatchdogIntHandler,                     // 34 Watchdog timer, timer 0
//    Timer0AIntHandler,                      // 35 Timer 0 subtimer A
//    Timer0BIntHandler,                      // 36 Timer 0 subtimer B
//    Timer1AIntHandler,                      // 37 Timer 1 subtimer A
//    Timer1BIntHandler,                      // 38 Timer 1 subtimer B
//    Timer2AIntHandler,                      // 39 Timer 2 subtimer A
//    Timer2BIntHandler,                      // 40 Timer 2 subtimer B
//    CompIntHandler,                         // 41 Analog Comparator 0
//    RFCoreTxIntHandler,                     // 42 RFCore Rx/Tx
//    RFCoreErrIntHandler,                    // 43 RFCore Error
//    IcePickIntHandler,                      // 44 IcePick
//    FlashIntHandler,                        // 45 FLASH Control
//    AESIntHandler,                          // 46 AES
//    PKAIntHandler,                          // 47 PKA
//    SleepModeIntHandler,                    // 48 Sleep Timer
//    MacTimerIntHandler,                     // 49 MacTimer
//    SSI1IntHandler,                         // 50 SSI1 Rx and Tx
//    Timer3AIntHandler,                      // 51 Timer 3 subtimer A
//    Timer3BIntHandler,                      // 52 Timer 3 subtimer B
//    0,                                      // 53 Reserved
//    0,                                      // 54 Reserved
//    0,                                      // 55 Reserved
//    0,                                      // 56 Reserved
//    0,                                      // 57 Reserved
//    0,                                      // 58 Reserved
//    0,                                      // 59 Reserved
//    USBIntHandler,                          // 60 USB 2538
//    0,                                      // 61 Reserved
//    uDMAIntHandler,                         // 62 uDMA
//    uDMAErrIntHandler,                      // 63 uDMA Error
//#ifndef CC2538_USE_ALTERNATE_INTERRUPT_MAP
//    0,                                      // 64 64-155 are not in use
//    0,                                      // 65
//    0,                                      // 66
//    0,                                      // 67
//    0,                                      // 68
//    0,                                      // 69
//    0,                                      // 70
//    0,                                      // 71
//    0,                                      // 72
//    0,                                      // 73
//    0,                                      // 74
//    0,                                      // 75
//    0,                                      // 76
//    0,                                      // 77
//    0,                                      // 78
//    0,                                      // 79
//    0,                                      // 80
//    0,                                      // 81
//    0,                                      // 82
//    0,                                      // 83
//    0,                                      // 84
//    0,                                      // 85
//    0,                                      // 86
//    0,                                      // 87
//    0,                                      // 88
//    0,                                      // 89
//    0,                                      // 90
//    0,                                      // 91
//    0,                                      // 92
//    0,                                      // 93
//    0,                                      // 94
//    0,                                      // 95
//    0,                                      // 96
//    0,                                      // 97
//    0,                                      // 98
//    0,                                      // 99
//    0,                                      // 100
//    0,                                      // 101
//    0,                                      // 102
//    0,                                      // 103
//    0,                                      // 104
//    0,                                      // 105
//    0,                                      // 106
//    0,                                      // 107
//    0,                                      // 108
//    0,                                      // 109
//    0,                                      // 110
//    0,                                      // 111
//    0,                                      // 112
//    0,                                      // 113
//    0,                                      // 114
//    0,                                      // 115
//    0,                                      // 116
//    0,                                      // 117
//    0,                                      // 118
//    0,                                      // 119
//    0,                                      // 120
//    0,                                      // 121
//    0,                                      // 122
//    0,                                      // 123
//    0,                                      // 124
//    0,                                      // 125
//    0,                                      // 126
//    0,                                      // 127
//    0,                                      // 128
//    0,                                      // 129
//    0,                                      // 130
//    0,                                      // 131
//    0,                                      // 132
//    0,                                      // 133
//    0,                                      // 134
//    0,                                      // 135
//    0,                                      // 136
//    0,                                      // 137
//    0,                                      // 138
//    0,                                      // 139
//    0,                                      // 140
//    0,                                      // 141
//    0,                                      // 142
//    0,                                      // 143
//    0,                                      // 144
//    0,                                      // 145
//    0,                                      // 146
//    0,                                      // 147
//    0,                                      // 148
//    0,                                      // 149
//    0,                                      // 150
//    0,                                      // 151
//    0,                                      // 152
//    0,                                      // 153
//    0,                                      // 154
//    0,                                      // 155
//    USBIntHandler,                          // 156 USB
//    RFCoreTxIntHandler,                     // 157 RFCORE RX/TX
//    RFCoreErrIntHandler,                    // 158 RFCORE Error
//    AESIntHandler,                          // 159 AES
//    PKAIntHandler,                          // 160 PKA
//    SleepModeIntHandler,                    // 161 SMTimer
//    MacTimerIntHandler,                     // 162 MACTimer
//#endif
//} IRQn_Type;